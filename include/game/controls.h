
#ifndef NIGHTFALL_CONTROLS_H
#define NIGHTFALL_CONTROLS_H

#include <stdint.h>

typedef struct controls {
    uint8_t up : 1;
    uint8_t down : 1;
    uint8_t left : 1;
    uint8_t right : 1;
    uint8_t b : 1;
    uint8_t a : 1;
    uint8_t start : 1;
    uint8_t select : 1;
} controls_st;

#define CONTROL_PRESSED 1
#define CONTROL_NOTPRESSED 0

#endif
