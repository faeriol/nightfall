
#ifndef NIGHTFALL_PLAYER_H
#define NIGHTFALL_PLAYER_H

#include "coredefs.h"
#include <stdint.h>

#define WHIP_STOCK 0x00
#define WHIP_SHORT 0x01
#define WHIP_LONG 0x02
#define WHIP_FLAMING 0x03

#define MUL_ZERO 0x01
#define MUL_DOUBLE 0x02
#define MUL_TRIPPLE 0x03

typedef struct player player_t;

void PlayerReset(player_t *player);

player_t *PlayerNew(void);

void PlayerFree(player_t *player);

void PlayerHit(player_t *player, health_t damage);

seconds_t PlayerInvincible(player_t *player);

void PlayerCountdownInvincible(player_t *player);

uint8_t PlayerWhip(player_t *player);

uint8_t PlayerPowerupWhip(player_t *player);

#endif
