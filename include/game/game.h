
#ifndef NIGHTFALL_GAME_H
#define NIGHTFALL_GAME_H

#include "controls.h"
#include "coredefs.h"
#include <stdint.h>

typedef uint16_t gclock_t;

typedef uint16_t score_t;

typedef struct gamestate gamestate_t;

gamestate_t *GamestateNew(void);

void GamestateFree(gamestate_t *state);

void GamestateNewPlayer(gamestate_t *state);

state_t GamestateState(gamestate_t *state);
void GamestateSetState(gamestate_t *gamestate, state_t state);
state_t GamestateFlipPause(gamestate_t *state);
void GamestateStep(gamestate_t *state, controls_st controls, uint8_t dt);

#endif
