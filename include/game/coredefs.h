
#ifndef NIGHTFALL_COREDEFS_H
#define NIGHTFALL_COREDEFS_H

#include <stdint.h>

typedef uint8_t health_t;
#define HEALTH_MAX (health_t)0x80 /*Enough*/

typedef uint8_t state_t;

#define STATE_DONE (state_t)0x0
#define STATE_PAUSE (state_t)0x1
#define STATE_RUN (state_t)0x2

typedef uint8_t seconds_t;

typedef uint8_t hearts_t;
#define HEART_MIN (hearts_t)0
#define HEART_START (hearts_t)5
#define HEART_MAX (hearts_t)99

#endif
