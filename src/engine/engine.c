#include "engine/engine.h"
#include <stdio.h>
#include <stdlib.h>

#include "game/game.h"

#include "SDL2/SDL.h"

#include "game/controls.h"

#define BASE_TIMESTEP 10

static SDL_GameController *pad;
static SDL_Joystick *joy;

static SDL_Window *window = NULL;
static SDL_Renderer *renderer = NULL;

static const controls_st zc = {0};

static controls_st controls = {0};

static inline void ProcessControllerButton(gamestate_t *state,
                                           SDL_ControllerButtonEvent *event) {
    switch (event->state) {
    case SDL_PRESSED:
        switch (event->button) { /*Hard code for XBox controller for now*/
        case 0:
            ++controls.b;
            break;
        case 1:
            ++controls.a;
            break;
        case 4:
            ++controls.select;
            break;
        case 6:
            ++controls.start;
            break;
        case 11:
            ++controls.up;
            break;
        case 12:
            ++controls.down;
            break;
        case 13:
            ++controls.left;
            break;
        case 14:
            ++controls.right;
            break;
        default:
            /* printf("Button pressed: %d\n", event->button); */ /*Debug*/
            break;
        }
        break;
    case SDL_RELEASED:
        /* printf("Button released: %d\n", event->button); */
        break;
    default:
        printf("Should not get here\n");
        break;
    }
}

void ProcessEvents(gamestate_t *state, uint32_t time) {
    GamestateStep(state, controls, time);
}

void ProcessInput(gamestate_t *state) {

    SDL_Event event;
    controls = zc;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_QUIT:
            GamestateSetState(state, STATE_DONE);
            break;
        case SDL_WINDOWEVENT:
            /*Focus, mouse in/out, etc */
            break;
        case SDL_MOUSEBUTTONDOWN:
            break;
        case SDL_MOUSEBUTTONUP:
            break;
        case SDL_KEYDOWN:
            break;
        case SDL_KEYUP:
            break;
        case SDL_CONTROLLERDEVICEADDED:
            (void)SDL_GameControllerEventState(SDL_ENABLE);
            printf("Controller detected: %d\n", event.cdevice.which);
            pad = SDL_GameControllerOpen(event.cdevice.which);
            if (NULL != pad) {
                joy = SDL_GameControllerGetJoystick(pad);
            }
            break;
        case SDL_CONTROLLERDEVICEREMOVED:
            printf("Controller removed\n");
            SDL_GameControllerClose(pad);
            break;
        case SDL_JOYAXISMOTION:
        case SDL_CONTROLLERAXISMOTION:
            break; /*Dont care*/
        case SDL_JOYHATMOTION:
            break; /*Apparently also handled as button*/
        case SDL_JOYBUTTONDOWN:
            break;
        case SDL_JOYBUTTONUP:
            break;
        case SDL_CONTROLLERBUTTONUP:
        case SDL_CONTROLLERBUTTONDOWN:
            ProcessControllerButton(state, &(event.cbutton));
            break;
        case SDL_TEXTINPUT:
            break; /* Who would care?*/
        case SDL_MOUSEMOTION:
            break;
        default:
            printf("Event type: %d\n", event.type);
            break;
        }
    }
}

void Render() {
    // printf("in Render()\n");
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
}

int EngineInit() {

    if (0 != SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER)) {
        printf("Couldn't init SDL: %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }
    window =
        SDL_CreateWindow("Castlevania", SDL_WINDOWPOS_UNDEFINED,
                         SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_OPENGL);
    if (NULL == window) {
        SDL_Quit();
        return EXIT_FAILURE;
    }
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (NULL == renderer) {
        SDL_Quit();
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void Clean() {
    if (NULL != pad) {
        SDL_GameControllerClose(pad);
        pad = NULL;
        joy = NULL;
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

static Uint32 ticks;

void StartCount(void) { ticks = SDL_GetTicks(); }

uint32_t GetTime(void) { return (uint32_t)(SDL_GetTicks() - ticks); }

/**
 * @brief yield to the system to be nice and not hammer the CPU with a infinite
 * loop
 */
static inline void yield(void) { SDL_Delay(0); }

int Run(void) {

    gamestate_t *state = GamestateNew();
    uint32_t ticks;
    uint32_t accumulator = 0;

    StartCount();
    while (STATE_DONE != GamestateState(state)) {

        // Flip this on head! Only update game state if amount of time X has
        // passed!
        // Then render new state!
        // That way simulation can advance in steps. And then if anything lags,
        // its
        // rendering itself
        // Still process input every loop!?!? No SDL_Event is queue. Let system
        // work
        // for us

        while (BASE_TIMESTEP <= accumulator) {
            ProcessInput(state);
            ProcessEvents(state, BASE_TIMESTEP);
            Render();
            accumulator -= BASE_TIMESTEP;
        }
        ticks =
            GetTime(); // Might want to still give time to system, but compute
                       // how much system took...
        accumulator += ticks;
        StartCount();
        // printf("Loop time: %d\n", ticks);
        yield();
    }
    GamestateFree(state);
    return 0;
}
