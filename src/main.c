#include <stdio.h>
#include <stdlib.h>

#include "engine/engine.h"

int main(int argc, char **argv) {

    if (EngineInit() != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }

    (void)Run();

    Clean();

    return EXIT_SUCCESS;
}
