#include "game/player.h"
#include "game/coredefs.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct power {
    uint8_t whip : 3;
    uint8_t mul : 3;
} power_t;

typedef uint8_t weapon_t;

#define WEAPON_NONE (weapon_t)0x00
#define WEAPON_KNIFE (weapon_t)0x01
#define WEAPON_BOOMRANG (weapon_t)0x02
#define WEAPON_HOLYW (weapon_t)0x04
#define WEAPON_AXE (weapon_t)0x08
#define WEAPON_WATCH (weapon_t)0x10

#define INVINCIBLE_NOT (seconds_t)0x0
#define INVINCIBLE_HURT (seconds_t)0x04
#define INVINCIBLE_POTION (seconds_t)0x10

struct player {
    health_t life;
    hearts_t hearts;
    power_t powerup;
    weapon_t throwable;
    seconds_t invincible;
};

#define PLAYER_NEW                                                             \
    {                                                                          \
        HEALTH_MAX, HEART_START, {WHIP_STOCK, MUL_NONE}, WEAPON_NONE,          \
            INVINCIBLE_NOT                                                     \
    }

void PlayerReset(player_t *player) {
    player->life = HEALTH_MAX;
    player->hearts = HEART_START;
    player->powerup.whip = WHIP_STOCK;
    player->powerup.mul = MUL_ZERO;
    player->throwable = WEAPON_NONE;
    player->invincible = INVINCIBLE_NOT;
}

player_t *PlayerNew(void) {
    player_t *player = (player_t *)malloc(sizeof(player_t));
    if (NULL != player) {
        PlayerReset(player);
    }
    return player;
}

void PlayerFree(player_t *player) { free(player); }

void PlayerHit(player_t *player, health_t damage) {
    if (0 == player->invincible) {
        player->life -= damage;
        player->invincible = INVINCIBLE_HURT;
    }
}

seconds_t PlayerInvincible(player_t *player) { return player->invincible; }

void PlayerCountdownInvincible(player_t *player) {
    if (INVINCIBLE_NOT < player->invincible) {
        --player->invincible;
    }
}

uint8_t PlayerWhip(player_t *player) { return player->powerup.whip; }

uint8_t PlayerPowerupWhip(player_t *player) {
    if (WHIP_LONG > player->powerup.whip) {
        ++player->powerup.whip;
    } else {
        printf("Why is game still giving upgrades?\n");
    }
    return player->powerup.whip;
}
