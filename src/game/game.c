#include "game/game.h"
#include "game/player.h"
#include <stdio.h>
#include <stdlib.h>

#define CLOCK_MIN 0
#define CLOCK_MAX 500

struct gamestate {
    player_t *player;
    uint8_t lives; // Dont care, for now
    uint8_t stage; // Dont care, for now
    score_t score;
    gclock_t time; // Dont care, for now
    health_t ennemy;
    state_t state;
};

#define LIVES_DEFAULT 3
#define TIME_DEFAULT 400

gamestate_t *GamestateNew(void) {
    gamestate_t *gamestate = malloc(sizeof(gamestate_t));
    if (NULL != gamestate) {
        gamestate->player = PlayerNew();
        gamestate->lives = LIVES_DEFAULT;
        gamestate->state = 0;
        gamestate->score = 0;
        gamestate->time = TIME_DEFAULT;
        gamestate->ennemy = HEALTH_MAX;
        gamestate->state = STATE_RUN;
    }
    return gamestate;
}

void GamestateFree(gamestate_t *state) {
    PlayerFree(state->player);
    free(state);
}

state_t GamestateState(gamestate_t *state) { return state->state; }

void GamestateSetState(gamestate_t *gamestate, state_t state) {
    gamestate->state = state;
}

state_t GamestateFlipPause(gamestate_t *state) {
    if (STATE_RUN == state->state) {
        state->state = STATE_PAUSE;
    } else if (STATE_PAUSE == state->state) {
        state->state = STATE_RUN;
    }
    return state->state;
}

void GamestateNewPlayer(gamestate_t *state) { PlayerReset(state->player); }

void GamestateStep(gamestate_t *state, controls_st controls, uint8_t dt) {
    if (1 == controls.start) {
        if (STATE_PAUSE == GamestateFlipPause(state)) {
            printf("Pausing simulation\n");
            return;
        } else {
            printf("Simulation running again\n");
        }
    }
}
