#!/usr/bin/env bash

echo "Formatting Code"
find . -name '*.[ch]' | xargs clang-format -i $FOPTS
