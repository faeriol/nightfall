engine = src/engine
enginesrc = $(engine)/engine.c

game = src/game
gamesrc = $(game)/game.c $(game)/player.c 

all: format
	clang -g -Iinclude src/main.c $(enginesrc) $(gamesrc) -lSDL2 -Wall -pedantic

format:
	./format.sh
